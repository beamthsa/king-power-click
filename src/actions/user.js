import * as userServices from '@services/user';

export const UPDATE_CURRENT_PAGE = 'UPDATE_CURRENT_PAGE';

export const CREATE_USER = 'CREATE_USER';
export const UPDATE_USER = 'UPDATE_USER';
export const DELETE_USER = 'DELETE_USER';
export const GET_USER = 'GET_USER';
export const SAVE_USER = 'SAVE_USER';
export const RECEIVE_USERS = 'RECEIVE_USERS';

export const updateCurrentPage = (data) => {
    return {
        type: UPDATE_CURRENT_PAGE,
        payload: data
    };
};

export const createUser = (data) => {
    const result = userServices.createUser(data);
    return {
        type: CREATE_USER,
        payload: result
    };
};

export const getUser = (id) => {
    const result = userServices.getUserById(id);
    return {
        type: SAVE_USER,
        payload: result.data
    };
}

export const clearUser = () => {
    return {
        type: SAVE_USER,
        payload: null
    };
}

export const updateUser = (id, data) => {
    const result = userServices.updateUserById(id, data);
    return {
        type: UPDATE_USER,
        payload: result
    };
};

export const deleteUser = (id) => {
    const result = userServices.deleteUserById(id);
    return {
        type: DELETE_USER,
        payload: result
    };
}

export const receiveUsers = ({data, total}) => {
    return {
        type: RECEIVE_USERS,
        payload: {
            users: data,
            totalUser: total
        }
    };
};

export function fetchUsers() {
    return (dispatch, getState) => {
        const {users} = getState();
        const {data, total} = userServices.getUsers({
            resultsPerPage: users.itemsPerPage,
            selectedPage: users.currentPage
        });
        return dispatch(receiveUsers({data, total}));
    }
}

export default {
    updateCurrentPage,
    createUser,
    getUser,
    clearUser,
    updateUser,
    deleteUser,
    fetchUsers
};

import React from 'react';
import {connect} from 'react-redux'
import {Table, Button, Popconfirm, notification} from 'antd';
import {QuestionCircleOutlined} from '@ant-design/icons';
import isEmpty from 'lodash/isEmpty';
import filter from 'lodash/filter';

import Container from '@components/Container'
import Layout from '@components/Layout'
import UserForm from '@components/UserForm'
import userActions from '@actions/user';
import MESSAGE from '@constants/message';

class HomePage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedId: null,
            selectedRowKeys: [],
            showCreateModal: false,
            showEditModal: false
        };
    }

    componentDidMount() {
        this.props.fetchUsers();
    }

    openCreateModal = () => this.setState({showCreateModal: true});

    closeCreateModal = () => this.setState({showCreateModal: false});

    openEditModal = (id) => {
        this.props.getUser(id);
        this.setState({
            selectedId: id,
            showEditModal: true
        });
    };

    closeEditModal = () => {
        this.props.clearUser();
        this.setState({
            selectedId: null,
            showEditModal: false
        });
    }

    onSelectChange = selectedRowKeys => {
        this.setState({selectedRowKeys});
    };

    onPageChange = (page) => {
        this.props.updateCurrentPage(page);
        this.props.fetchUsers();
    };

    onCreateUser = (data) => {
        this.props.createUser(data);
        this.props.fetchUsers();
        this.closeCreateModal();

        notification.info({
            message: MESSAGE['create_user_success']
        });
    };

    onEditUser = (data) => {
        const {selectedId} = this.state;

        const updatedData = Object.assign({}, this.props.selectedUser, data);
        this.props.updateUser(selectedId, updatedData);

        this.props.fetchUsers();
        notification.info({
            message: MESSAGE['update_user_success']
        });
        this.closeEditModal();
    }

    onDeleteUser(id) {
        this.props.deleteUser(id);
        notification.info({
            message: MESSAGE['delete_user_success']
        });
        this.props.fetchUsers();

        this.setState(({selectedRowKeys}) => {
            const targetIds = typeof id === 'string' ? [id] : id;
            return {
                selectedRowKeys: filter(selectedRowKeys, value => !targetIds.includes(value))
            };
        }, () => console.log(this.state.selectedRowKeys));
    }

    renderText = (text) => text || '-';

    getTableColumns() {
        return [
            {
                title: 'Name',
                key: 'name',
                render: ({firstName = '', lastName = ''}) => this.renderText(`${firstName} ${lastName}`.trim()),
            },
            {
                title: 'Gender',
                key: 'gender',
                dataIndex: 'gender',
                render: this.renderText
            },
            {
                title: 'Mobile Phone',
                key: 'phone',
                render: ({phonePrefix = '', phone = ''}) => this.renderText(`${!isEmpty(phonePrefix) && `+${phonePrefix}`}${phone}`)
            },
            {
                title: 'Nationality',
                key: 'nationality',
                dataIndex: 'nationality',
                render: this.renderText
            },
            {
                key: 'action',
                render: ({id}) => (
                    <>
                        <Button type="link" onClick={() => this.openEditModal(id)}>
                            Edit
                        </Button>
                        <Popconfirm
                            title="Are you sure?"
                            okText="Confirm"
                            okButtonProps={{
                                type: 'primary',
                                danger: true
                            }}
                            cancelText="Cancel"
                            placement="bottom"
                            onConfirm={() => this.onDeleteUser(id)}
                            icon={<QuestionCircleOutlined style={{color: 'red'}}/>}
                        >
                            <Button type="link" danger>
                                Delete
                            </Button>
                        </Popconfirm>
                    </>
                ),
            }
        ]
    }

    render() {
        const {showCreateModal, showEditModal, selectedRowKeys} = this.state;
        const {users, totalUser, currentPage, itemsPerPage, selectedUser} = this.props;

        return (
            <>
                <Layout className="home-page">
                    <Container>
                        <div className="home-actions">
                            <Popconfirm
                                title="Are you sure?"
                                okText="Confirm"
                                okButtonProps={{
                                    type: 'primary',
                                    danger: true
                                }}
                                cancelText="Cancel"
                                placement="right"
                                disabled={isEmpty(selectedRowKeys)}
                                onConfirm={() => this.onDeleteUser(selectedRowKeys)}
                                icon={<QuestionCircleOutlined style={{ color: 'red' }}/>}
                            >
                                <Button disabled={isEmpty(selectedRowKeys)} danger>
                                    Delete User
                                </Button>
                            </Popconfirm>
                            <Button onClick={this.openCreateModal}>
                                Create User
                            </Button>
                        </div>
                        <Table
                            rowKey="id"
                            rowSelection={{
                                selectedRowKeys,
                                type: 'checkbox',
                                onChange: this.onSelectChange
                            }}
                            columns={this.getTableColumns()}
                            pagination={{
                                total: totalUser,
                                current: currentPage,
                                pageSize: itemsPerPage,
                                onChange: this.onPageChange,
                                size: 'small'
                            }}
                            dataSource={users}
                        />
                    </Container>
                </Layout>

                {showCreateModal && (
                    <UserForm
                        modalProps={{
                            title: 'Create User',
                            onCancel: this.closeCreateModal,
                            okText: 'Submit',
                            cancelText: 'Cancel',
                            cancelButtonProps: {
                                danger: true
                            },
                            width: 1000,
                            visible: true
                        }}
                        onSubmit={this.onCreateUser}/>
                )}

                {showEditModal && (
                    <UserForm
                        modalProps={{
                            title: 'Edit User',
                            okText: 'Submit',
                            onCancel: this.closeEditModal,
                            cancelText: 'Cancel',
                            cancelButtonProps: {
                                danger: true
                            },
                            width: 1000,
                            visible: true
                        }}
                        initialValues={selectedUser}
                        onSubmit={this.onEditUser}/>
                )}

                <style jsx global>{`
                    .home-page {
                        padding: 2rem 0;
                    }

                    .home-actions {
                        display: flex;
                        justify-content: space-between;
                        padding-bottom: 1rem;
                    }
                `}</style>
            </>
        );
    }
}

function mapStateToProps(state) {
    const {users, totalUser, currentPage, itemsPerPage, selectedUser} = state.users;

    return {
        selectedUser,
        users,
        totalUser,
        currentPage,
        itemsPerPage
    };
}
function mapDispatchToProps(dispatch) {
    return {
        updateCurrentPage: (data) => dispatch(userActions.updateCurrentPage(data)),
        createUser: (data) => dispatch(userActions.createUser(data)),
        getUser: (data) => dispatch(userActions.getUser(data)),
        updateUser: (id, data) => dispatch(userActions.updateUser(id, data)),
        clearUser: (data) => dispatch(userActions.clearUser()),
        deleteUser: (data) => dispatch(userActions.deleteUser(data)),
        fetchUsers: () => dispatch(userActions.fetchUsers())
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);

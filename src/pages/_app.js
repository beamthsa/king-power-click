import {Provider} from 'react-redux';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'antd/dist/antd.css';

import configureStore from '@src/configureStore';

const store = configureStore();

function MyApp({Component, pageProps}) {
    return (
        <Provider store={store}>
            <Component {...pageProps}/>
        </Provider>
    )
}

export default MyApp;

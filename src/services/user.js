import filter from 'lodash/filter';
import find from 'lodash/find';
import findIndex from 'lodash/findIndex';
import isArray from 'lodash/isArray';
import {v4 as uuidv4} from 'uuid';

const LOCAL_STORAGE_KEY = '_USERS';

function _getUserFromLocalStorage() {
    const data = localStorage.getItem(LOCAL_STORAGE_KEY);
    return JSON.parse(data) || [];
}

function _saveUserFromLocalStorage(data) {
    localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(data));
}

function _getUserIndexById(data, id) {
    return findIndex(data, ['id', id]);
}

export function createUser(data) {
    try {
        const users = _getUserFromLocalStorage();
        const createdData = {
            id: uuidv4(),
            ...data
        };

        _saveUserFromLocalStorage([...users, createdData]);

        return {
            success: true,
            data: createdData
        };
    } catch (e) {
    }

    return {
        success: false,
        data: null
    }
}

export function updateUserById(id, data) {
    try {
        const users = _getUserFromLocalStorage();
        const targetIndex = _getUserIndexById(users, id);
        if (targetIndex >= 0) {
            users[targetIndex] = data;
            _saveUserFromLocalStorage(users);

            return {
                data,
                success: true
            };
        }
    } catch (e) {
    }

    return {
        success: false,
        data: null
    };
}

export function deleteUserById(id) {
    try {
        const ids = isArray(id) ? id : [id];
        const users = _getUserFromLocalStorage();

        const updatedUsers = filter(users, ({id}) => !ids.includes(id));
        _saveUserFromLocalStorage(updatedUsers);

        return {
            data: updatedUsers,
            success: true
        };
    } catch (e) {
        console.log(e);
    }

    return {
        success: false,
        data: null
    };
}

export function getUserById(id) {
    const users = _getUserFromLocalStorage();
    const targetUser = find(users, ['id', id]);

    if (targetUser) {
        return {
            success: true,
            data: targetUser
        }
    }

    return {
        success: false,
        data: null
    };
}

export function getUsers(query = {}) {
    const {resultsPerPage = 10, selectedPage = 1} = query;

    const users = _getUserFromLocalStorage();
    const startIndex = parseInt((selectedPage - 1) * resultsPerPage);
    const targetData = [...users];

    return {
        data: targetData.splice(startIndex, resultsPerPage),
        total: users.length || 0
    };
}

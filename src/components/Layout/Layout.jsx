import React from 'react';
import Head from 'next/head';
import {Layout as AntLayout} from 'antd';

import Container from '@components/Container'

const {Header, Content, Footer} = AntLayout;

const Layout = ({children, ...props}) => (
    <>
        <Head>
            <title>King Power Click</title>
            <link rel="icon" href="/favicon.ico" />
        </Head>
        <AntLayout>
            <Header>
                <Container>
                    <img className="brand" src="logo.png" />
                </Container>
            </Header>
            <Content {...props}>
                {children}
            </Content>
            <Footer>
                <Container>
                    <a
                        href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                        {`Powered by `}
                        <img src="/vercel.svg" alt="Vercel Logo" className="logo" />
                    </a>
                </Container>
            </Footer>
        </AntLayout>

        <style jsx global>{`
            html,
            body {
                padding: 0;
                margin: 0;
                font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto,
                    Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue,
                    sans-serif;
            }

            a {
                color: inherit;
                text-decoration: none;
            }

            .ant-layout,
            .ant-layout-header,
            .ant-layout-footer {
                background-color: #FFF;
            }

            .ant-layout-header {
                box-shadow: 0px 0px 5px #bbb;
            }

            .ant-layout-footer {
                text-align: center;
            }

            .brand {
                height: 43px;
            }

            .logo {
                height: 0.875em;
            }
        `}</style>
    </>
);

export default Layout;

import React from 'react';
import {Row, Col, Modal, Form, Input, Select, Radio, DatePicker, Space} from 'antd';
import map from 'lodash/map';
import sortBy from 'lodash/sortBy';
import entries from 'lodash/entries';
import Flag from 'react-world-flags'
import moment from 'moment';

import {TITLE_OPTIONS, GENDER_OPTIONS} from '@constants/userForm';
import COUNTRY_PHONE from '@constants/phone';
import COUNTRY from '@constants/country';

const UserForm = ({onSubmit, modalProps, initialValues, ...props}) => {
    const [form] = Form.useForm();

    const countryPhones = map(entries(COUNTRY_PHONE), ([key, value]) => ({name: value, value: key}));
    const countries = map(entries(COUNTRY), ([key, value]) => ({name: value, value: key}));
    const countryNames = sortBy(countries, 'name');

    const PhonePrefix = (
        <Form.Item name="phonePrefix" noStyle>
            <Select style={{width: 120}} showSearch>
                {map(countryPhones, data => (
                    <Select.Option key={data.value} value={data.name} title={data.name}>
                        <Space>
                            <Flag code={data.value} height="12" />
                            {`+${data.name}`}
                        </Space>
                    </Select.Option>
                ))}
            </Select>
        </Form.Item>
    );

    if (initialValues && initialValues.birthDate) {
        initialValues.birthDate = moment.utc(initialValues.birthDate);
    }

    return (
        <>
            <Modal
                {...modalProps}
                onOk={async () => {
                    try {
                        await form.validateFields();

                        const {birthDate, ...fieldsValue} = form.getFieldsValue();
                        onSubmit({
                            birthDate: birthDate.utc().format(),
                            ...fieldsValue
                        });
                    } catch (e) {}
                }}>
                <Form
                    {...props}
                    initialValues={initialValues}
                    form={form}
                    validateMessages={{
                        required: 'This field is required',
                        types: {
                            number: 'This field is not a valid ${type}'
                        },
                        string: {
                            max: 'This field cannot be longer than ${max}'
                        },
                        number: {
                            max: 'This field cannot be longer than ${max}'
                        }
                    }}>
                    <Row>
                        <Col xs={24} md={6}>
                            <Form.Item
                                label="Title"
                                name="title"
                                rules={[{required: true}]}
                                required>
                                <Select>
                                    {map(TITLE_OPTIONS, value => (
                                        <Select.Option key={value} value={value}>
                                            {value}
                                        </Select.Option>
                                    ))}
                                </Select>
                            </Form.Item>
                        </Col>
                        <Col xs={24} md={9}>
                            <Form.Item
                                label="First name"
                                name="firstName"
                                rules={[{required: true}]}
                                required>
                                <Input />
                            </Form.Item>
                        </Col>
                        <Col xs={24} md={9}>
                            <Form.Item
                                label="Last name"
                                name="lastName"
                                rules={[{required: true}]}
                                required>
                                <Input />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={24} md={8}>
                            <Form.Item
                                label="Birthday"
                                name="birthDate"
                                rules={[{required: true}]}
                                required>
                                <DatePicker style={{width: '100%'}} format="MM/DD/YYYY" placeholder="mm/dd/yyyy"/>
                            </Form.Item>
                        </Col>
                        <Col span={16}>
                            <Form.Item label="Nationality" name="nationality">
                                <Select placeholder="Please select" style={{width: '100%', maxWidth: 510}} showSearch>
                                    {map(countryNames, data => (
                                        <Select.Option key={data.value} value={data.value}>
                                            {data.name}
                                        </Select.Option>
                                    ))}
                                </Select>
                            </Form.Item>
                        </Col>
                    </Row>
                    <Form.Item
                        label="Citizen ID"
                        name="citizenId"
                        rules={[{max: 11}]}
                    >
                        <Input type="number" maxLength={11} style={{width: '100%', maxWidth: 510}} />
                    </Form.Item>
                    <Form.Item label="Gender" name="gender">
                        <Radio.Group>
                            {map(GENDER_OPTIONS, value => (
                                <Radio key={value} value={value}>{value}</Radio>
                            ))}
                        </Radio.Group>
                    </Form.Item>
                    <Form.Item
                        name="phone"
                        label="Mobile Phone"
                        rules={[{
                            required: true,
                            max: 15
                        }]}
                    >
                        <Input type="number" maxLength={15} addonBefore={PhonePrefix} style={{maxWidth: 510}} />
                    </Form.Item>
                    <Form.Item label="Passport No." name="passportNo">
                        <Input style={{maxWidth: 510}}/>
                    </Form.Item>
                    <Form.Item
                        label="Expected salary"
                        name="salary"
                        rules={[{
                            required: true,
                            max: 15
                        }]}
                        required>
                        <Input type="number" min="0" suffix="THB" style={{maxWidth: 510}} />
                    </Form.Item>
                </Form>
            </Modal>

            <style jsx global>{`
                .ant-form-item-control-input-content {
                    padding-right: 8px;
                }
            `}</style>
        </>
    )
};

UserForm.defaultProps = {
    modalProps: {}
};

export default UserForm;

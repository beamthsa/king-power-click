import React from 'react';
import classNames from 'classnames';

const Container = ({className, ...props}) => (
    <div className={classNames('container', className)} {...props}/>
);

export default Container;

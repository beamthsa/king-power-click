import {CREATE_USER, SAVE_USER, UPDATE_USER, DELETE_USER, RECEIVE_USERS, UPDATE_CURRENT_PAGE} from '@actions/user';

const defaultState = {
    users: [],
    selectedUser: null,
    totalUser: 0,
    currentPage: 1,
    itemsPerPage: 10
};

const users = (state = defaultState, action) => {
    switch (action.type) {
        case UPDATE_CURRENT_PAGE:
            return Object.assign({}, state, {
                currentPage: action.payload
            });
        case SAVE_USER:
            return Object.assign({}, state, {
                selectedUser: action.payload
            });
        case RECEIVE_USERS:
            return Object.assign({}, state, action.payload);
        case CREATE_USER:
        case UPDATE_USER:
        case DELETE_USER:
        default:
            return state;
    }
};

export default users;

export const TITLE_OPTIONS = ['Mr.', 'Mrs.', 'Ms.', 'Miss'];

export const GENDER_OPTIONS = ['Male', 'Female', 'Unisex']
